package org.ifi.seal.example;

public class Demo {
  private int x;
  public Demo(int x) {
    this.x = x;
  }
  public void run() {
    for (int i = 1; i < x; i++) {
      if (i % 3 == 0 && i % 5 == 0) {
        System.out.println(i);
      }
    }
  }
}

