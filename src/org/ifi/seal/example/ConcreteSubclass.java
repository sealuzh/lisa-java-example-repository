package org.ifi.seal.example;

public class ConcreteSubclass extends ConcreteClass {
  @Override
  public int returnsSomethingMethod() { return 101; }
}
