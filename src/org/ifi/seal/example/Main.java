package org.ifi.seal.example;

import static java.lang.Math.*;
import org.ifi.seal.example.subpackage.*;
import org.ifi.seal.example.subpackage.subsubpackage.*;
import org.ifi.seal.example_sibling.SiblingClass;
import org.ifi.seal.example.codesmells.*;

public class Main {
  public static void main(String[] args) {

    GenericPair<Integer, String> pair = new GenericPair<Integer, String>(1, "hello");
    GenericPair<Integer, String> qualifiedPair = new org.ifi.seal.example.GenericPair<Integer, String>(1, "hello");
    Integer key = pair.getKey();
    String value = pair.getValue();
    assert key.equals(1);
    assert value.equals("hello");

    ConcreteClass instance = new ConcreteClass();
    ConcreteClass instanceConstructedWithParams = new ConcreteClass(1);
    SiblingClass instanceSibling = new SiblingClass();
    SubpackageClass instanceSubpackage = new SubpackageClass();
    SubsubpackageClass instanceSubsubpackage = new SubsubpackageClass();
    ConcreteClass.InnerClass instanceInnerClass = instance.new InnerClass();
    ConcreteClass.InnerClass.InnerInnerClass instanceInnerInnerClass = instanceInnerClass.new InnerInnerClass();
    ConcreteClass nullVar = null;

    ControlFlow cf = new ControlFlow();

    CodeSmell brainMethod = new BrainMethod();

    instance.invokesMethodMethod(); // need to resolve to actual method in class

    double one = cos(PI * 2);
    float two = 2f;


    int x = 10;
    while (x > 0) {
      x--;
      --x;
    }
    assert(x <= 0);

    boolean assertionThing = false;
    if (1 > 2) {
    }
    else {
      if (200 > 100) {
        if (2000 > 1000) {
          assertionThing = true;
        }
      }
    }
    assert(assertionThing == true);

    assert(instance.returnsSomethingMethod() == 1);
    instance = new ConcreteSubclass();
    assert(instance.returnsSomethingMethod() == 101);

    System.out.println("Main function finished");

    Demo d = new Demo(40);
    d.run();
  }
}

