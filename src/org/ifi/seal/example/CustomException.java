package org.ifi.seal.example;

public class CustomException extends Exception {
  public CustomException(String message) {
    super(message);
  }
}

