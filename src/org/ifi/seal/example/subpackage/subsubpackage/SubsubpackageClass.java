package org.ifi.seal.example.subpackage.subsubpackage;

public class SubsubpackageClass {
  public int publicSiblingVariable = 1;
  public SubsubpackageClass() {
    int privateSiblingVariable = 1;
  }
}
