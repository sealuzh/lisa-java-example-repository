package org.ifi.seal.example.subpackage;

public class SubpackageClass {
  public int publicSiblingVariable = 1;
  public SubpackageClass() {
    int privateSiblingVariable = 1;
  }
}
