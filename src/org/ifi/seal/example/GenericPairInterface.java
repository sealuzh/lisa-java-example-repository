package org.ifi.seal.example;

public interface GenericPairInterface<K, V> {
  public K getKey();
  public V getValue();
}

